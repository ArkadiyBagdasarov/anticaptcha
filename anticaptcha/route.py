import asyncio
import logging
import aiohttp_cors
from aiohttp import web
from aiohttp_swagger import setup_swagger

from api.views import server

ROUTES = (
    ('POST', '/api/anticaptcha/get_cookie/', server.GetCookieView),
    ('GET', '/api/anticaptcha/balance/', server.BalanceView),
)


async def get_app():
    app = web.Application(client_max_size=0)

    for ROUTE in ROUTES:
        app.router.add_route(*ROUTE)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    for route in list(app.router.routes()):
        cors.add(route)

    setup_swagger(app, swagger_url="/api/docs/")

    return app


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                        level=logging.INFO)

    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(get_app())

    # web.run_app(app, path='/app/serve.sock')
    web.run_app(app, port=8383)

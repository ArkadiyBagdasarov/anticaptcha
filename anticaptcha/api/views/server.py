from aiohttp import web
from aiohttp.log import web_logger
from aiohttp.web_response import json_response, Response

from api.serializers import server
from base.decorators import get_data
from base.libs import get_balance
from base.make_work import get_captcha_solution
from base.settings import websiteURL


class GetCookieView(web.View):
    """
    ---
    description: Метод получения печенья.
    tags:
    - server
    produces:
    - application/json
    parameters:
    - in: body
      name: body
      required: true
      schema:
        type: object
        properties:
          websiteKey:
            type: string
            default: "6LfwuyUTAAAAAOAmoS0fdqijC2PbbdH4kjq62Y1qq"
            required: true
          proxyAddress:
            type: string
            default: "149.56.140.20"
            required: true
          proxyPort:
            type: string
            default: "8080"
            required: true
          proxyLogin:
            type: string
            default: "test"
            required: false
          proxyPassword:
            type: string
            default: "test"
            required: false
          referer:
            type: string
            default: "https://www.google.com/"
            required: true
          q:
            type: string
            default: "EgSVOIwUGLXQ8NwFIhkA8aeDSzDERpsiYcmBqQqODA7wCi4jkEXTMgFy"
            required: true
    responses:
        "200":
            description: OK
        "400":
            description: bad request
    """

    serializer_class = server.DataSerializer

    @get_data
    async def post(self):
        result, errors = self.serializer_class().load(self.data)
        if isinstance(errors, Response):
            return errors

        token, critical, error = await get_captcha_solution(websiteURL, **result)

        if not token:
            web_logger.error(error)
            return json_response({'success': False,
                                  'error': error,
                                  'critical': critical})

        return json_response({'token': token, 'critical': critical, 'error': error})

        # proxy_login = result.get('proxyLogin')
        # proxy_password = result.get('proxyPassword')
        # proxy_auth = ''
        #
        # if proxy_login and proxy_password:
        #     proxy_auth = '{}:{}@'.format(proxy_login, proxy_password)
        #
        # html = await get_html(token, proxy_auth=proxy_auth, **result)
        #
        # if not html:
        #     web_logger.error('token not valid')
        #     return json_response({'success': False,
        #                           'error': 'token not valid',
        #                           'critical': False})
        #
        # _cookies, href = get_cookie_and_href(html)
        #
        # new_headers = headers.copy()
        #
        # del new_headers['Accept']
        #
        # c = _cookies.split("=", maxsplit=1)
        #
        # async with aiohttp.ClientSession(headers=new_headers, cookies={c[0]: c[1]}) as session:
        #     async with session.get(href, proxy="http://{}{}:{}".format(proxy_auth, result['proxyAddress'],
        #                                                                result['proxyPort'])) as resp:
        #         if resp.status != 200:
        #             return json_response({'success': False,
        #                                   'error': 'token not valid',
        #                                   'critical': False})
        #
        # json_cookies = get_dict_cookies(_cookies)
        # cookies = _cookies[_cookies.index("GOOGLE_ABUSE"):]
        # return json_response({'success': True, 'cookies': cookies, 'json_cookies': json_cookies})


class BalanceView(web.View):
    """
    ---
    description: Метод получения баланса.
    tags:
    - server
    produces:
    - application/json

    responses:
        "200":
            description: OK
    """

    async def get(self):
        balance = await get_balance()

        return json_response({'success': True, 'balance': balance['balance']})

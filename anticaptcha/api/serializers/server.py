from marshmallow import fields

from api.serializers.base import BaseSchema


class DataSerializer(BaseSchema):
    websiteKey = fields.Str(required=True)
    proxyAddress = fields.Str(required=True)
    proxyPort = fields.Str(required=True)
    proxyLogin = fields.Str(required=False, missing=None)
    proxyPassword = fields.Str(required=True, missing=None)
    referer = fields.Str(required=True)
    q = fields.Str(required=True)

from aiohttp.log import web_logger
from aiohttp.web_response import json_response
from marshmallow import Schema, ValidationError


class BaseSchema(Schema):
    def load(self, data, many=None, partial=None):
        result, errors = super(BaseSchema, self).load(data, many=many, partial=partial)

        if errors:
            web_logger.error("error serialize:(result) {}".format(result))
            web_logger.error("error serialize:(errors) {}".format(errors))
            return None, json_response({'success': False, 'error': errors})

        return result, None


def min_value(value):
    if value < 1:
        raise ValidationError('can not be less that 1')


def max_validate(max_value):
    def wraps(value):
        if len(value) > max_value:
            raise ValidationError('type not greater than {} symbols'.format(max_value))
    return wraps

import json
from functools import wraps

from aiohttp.log import web_logger
from aiohttp.web_response import json_response


def get_data(handler):
    @wraps(handler)
    async def wrap(request, *args, **kwargs):
        try:
            data = await request.request.json()
        except json.JSONDecodeError:
            web_logger.error('json decode error')
            return json_response({'success': False,
                                  'error': 'json decode error'})
        request.data = data

        return await handler(request, *args, **kwargs)
    return wrap

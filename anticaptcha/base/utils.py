from urllib import parse

import aiohttp
from aiohttp import FormData


def get_form_data(token, referer, q):
    data = FormData()

    data.add_field('g-recaptcha-response', token)
    data.add_field('continue', referer)
    data.add_field('q', q)

    return data


def get_cookie_and_href(html):
    from pyquery import PyQuery as pq
    divMain = pq(html)
    href = divMain('body').find('a').attr('href')

    google_abuse = parse.parse_qs(parse.urlparse(href).query)['google_abuse'][0]

    return google_abuse, href


async def get_html(token, referer, q, proxyAddress, proxyPort, proxy_auth, **kwargs):
    data = get_form_data(token, referer, q)
    new_headers = {
        "Host": "www.google.com",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1"
    }

    async with aiohttp.ClientSession(headers=new_headers) as session:
        async with session.post('https://www.google.com/sorry/index', data=data,
                                proxy="http://{}{}:{}".format(proxy_auth, proxyAddress, proxyPort),
                                allow_redirects=False) as resp:
            if resp.status != 302:
                return None

            html = await resp.text()

    return html


def get_dict_cookies(cookie):
    cookies = {}

    cookie_data = cookie.split('; ')

    for i in cookie_data:
        c = i.split("=", maxsplit=1)

        if 'GOOGLE_ABUSE_EXEMPTION' in i:
            cookies['name'] = c[0]
            cookies['value'] = c[1]
            continue

        if 'domain' in i:
            c[1] = '.' + c[1]

        cookies.update(dict((c,)))

    return cookies

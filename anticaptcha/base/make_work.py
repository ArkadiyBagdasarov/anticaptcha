from aiohttp.log import web_logger

from base.libs import aioNoCaptchaTask
from base.settings import ANTICAPTCHA_KEY


async def get_captcha_solution(websiteURL, websiteKey, proxyAddress,
                 proxyPort, proxyLogin=None, proxyPassword=None, **kwargs):
    try:
        result = await (aioNoCaptchaTask(ANTICAPTCHA_KEY).captcha_handler(websiteURL=websiteURL, websiteKey=websiteKey))

        web_logger.info(result)

        token = result.get('solution', {}).get('gRecaptchaResponse', '')

        if not token:
            critical, error = execute_error(result)
            return '', critical, error

        return token, False, ''

    except Exception as e:
        web_logger.error(e)
        return '', False, 'microservice error'


def execute_error(data):
    if 'errorCode' in data:

        if data['errorCode'] in ('ERROR_PROXY_BANNED', 'ERROR_PROXY_TRANSPARENT'):
            return True, data

    return False, data

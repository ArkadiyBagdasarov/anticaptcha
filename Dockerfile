FROM python:3.6.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /app
RUN mkdir /app/anticaptcha
COPY . /app/anticaptcha
WORKDIR /app/anticaptcha

RUN pip install -r /app/anticaptcha/deploy_assets/requirements.txt
